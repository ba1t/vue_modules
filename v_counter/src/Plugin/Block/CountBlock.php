<?php

namespace Drupal\v_counter\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a count block.
 *
 * @Block(
 *   id = "v_count_block",
 *   admin_label = @Translation("Count block"),
 *   category = @Translation("vue")
 * )
 */
class CountBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'count',
    ];
    $build['#attached']['library'][] = 'v_counter/store';

    return $build;
  }

}
