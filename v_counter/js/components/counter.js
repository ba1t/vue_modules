(function ($, Drupal, Vue) {
    Drupal.behaviors.vue__counter = {
      template: `
        <div>
          <div>Node ID: {{nodeId}}</div>
          <div>Counter: {{count}}</div>
          <div class="btn-group mt-4">
            <button class="btn btn-danger" v-if="count > 0" v-on:click="decrement">-</button>
            <button class="btn btn-success" v-on:click="increment">+</button>
          </div>
        </div>
      `,
      attach: function (context) {
        $(document, context).once('vue__counter_counter').each(() => {
          Vue.component('counter', {
            template: this.template,
            props: ['node-id'],
            data() {
              return {
                count: 0,
              }
            },
            methods: {
              increment() {
                this.$store.counter.increment();
                this.count += 1;
              },
              decrement() {
                this.$store.counter.decrement();
                this.count -= 1;
              }
            },
            mounted() {
              this.count = 0
            }
          });
        });
      }
    }
  } (jQuery, Drupal, Vue));
  