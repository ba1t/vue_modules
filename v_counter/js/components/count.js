(function ($, Drupal, Vue) {
    Drupal.behaviors.vue__counter_count = {
      template: `
        <div>Total of counter: {{ $store.counter.count }}</div>
      `,
      attach: function (context) {
        $(document, context).once('vue__counter_count').each(() => {
          Vue.component('count', {
            template: this.template,
          });
        })
      },
    };
  } (jQuery, Drupal, Vue));
  