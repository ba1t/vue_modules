(function (Vue) {
  const store = Vue.observable({
    counter: {
      count: 0,
      increment() {
        store.counter.count += 1;
      },
      decrement() {
        store.counter.count -= 1;
      }
    }
  })
  Vue.prototype.$store = Vue.prototype.$store || {};
  Vue.prototype.$store = {...Vue.prototype.$store, ...store};
})(Vue);
