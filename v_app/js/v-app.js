(function ($, Drupal, Vue) {
    Drupal.behaviors.v_app = {
      attach: function (context) {
        $(document, context).once('v_app').each(() => {
          const app = new Vue({
            el: context.querySelector('#page'),
            delimiters: ['${', '}'],
          });
        });
      }
    }
  } (jQuery, Drupal, Vue));
  