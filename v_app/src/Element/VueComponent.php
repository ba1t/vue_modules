<?php

namespace Drupal\v_app\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\RenderElement;

/**
 * @RenderElement("vue_component")
 */
class VueComponent extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    $class = get_class($this);
    return [
      '#component' => NULL,
      '#props' => NULL,
      '#process' => [
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
        [$class, 'preVueComponent'],
      ],
      '#theme_wrappers' => ['vue_component'],
    ];
  }


  /**
   * @param array $element
   * @return array
   */
  public static function preVueComponent(array $element): array {
    $element['#attributes'] = $element['#props'];

    return $element;
  }

}
