<?php

namespace Drupal\v_favorite\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a favorite block.
 *
 * @Block(
 *   id = "v_favorite_block",
 *   admin_label = @Translation("Favorite block"),
 *   category = @Translation("vue")
 * )
 */
class FavoriteBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#type' => 'vue_component',
      '#component' => 'favorite-count',
      '#props' => ['count' => 0],
    ];
    $build['#attached']['library'][] = 'v_favorite/count';
    $build['#cache'] = [
      'max-age' => 0
    ];
    return $build;
  }

}
