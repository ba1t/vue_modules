(function ($, Drupal, Vue) {
    Drupal.behaviors.vue__favorite_counter = {
      template: `
        <div>
          <span class="menu-item__count">{{ $store.favorite.count }}</span>
        </div>
      `,
      attach: function (context) {
        $(document, context).once('vue__favorite_count').each(() => {
          Vue.component('favorite-count', {
            template: this.template,
            name: 'favorite-count',
          });
        })
      },
    };
  } (jQuery, Drupal, Vue));
  