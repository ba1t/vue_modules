(function ($, Drupal, Vue) {
    Drupal.behaviors.vue__favorite_toggle = {
      template: `
        <div v-on:click="toggle">
          {{ toggleText }}
        </div>
      `,
      attach: function (context) {
        $(document, context).once('vue__favorite_toggle').each(() => {
          Vue.component('favorite-toggle', {
            template: this.template,
            props: ['node-id'],
            data() {
              return {
                inFavorite: false,
              }
            },
            computed: {
              toggleText() {
                return this.inFavorite ? Drupal.t('Usuń z ulubionych') : Drupal.t('Dodaj do ulubionych')
              }
            },
            methods: {
              toggle() {
                if(this.inFavorite) {
                    this.$store.favorite.removeFromFavorite()
                    this.inFavorite = false;
                }
                else {
                    this.$store.favorite.addToFavorite()
                    this.inFavorite = true;
                }
              }
            },
            mounted() {
              this.inFavorite = false
            }
          });
        });
      }
    }
  } (jQuery, Drupal, Vue));
  