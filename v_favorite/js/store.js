(function (Vue) {
    const store = Vue.observable({
      favorite: {
        count: 0,
        addToFavorite() {
          store.favorite.count += 1;
        },
        removeFromFavorite() {
          store.favorite.count -= 1;
        },
      }
    })
    Vue.prototype.$store = Vue.prototype.$store || {};
    Vue.prototype.$store = {...Vue.prototype.$store, ...store};
  })(Vue);
  